import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input() empleado:any
  @Output() agregarEmpleado: EventEmitter<any> = new EventEmitter()

  empleadoForm:FormGroup
  brm:string
  nombre:string
  puesto:string
  foto:any
  

  constructor(private formBuilder:FormBuilder) { }

  ngOnInit() {
    this.empleadoForm = this.formBuilder.group({
      brm: [''],
      nombre: [''],
      puesto:[''],
      foto:[null]
    });
  }

  postEmpleado():void{
    const empleado = {
      brm: this.empleadoForm.get('brm').value,
      nombre: this.empleadoForm.get('nombre').value,
      puesto:this.empleadoForm.get('puesto').value,
      foto:this.empleadoForm.get('foto').value,
    }    
    this.agregarEmpleado.emit(empleado)
  }

  onImageUpload(event){
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.empleadoForm.get('foto').setValue(file)
    }
  }
}
