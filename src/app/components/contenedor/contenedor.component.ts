import { Component, OnInit } from '@angular/core';

import { EmpleadoService } from 'src/app/services/empleado-service.service';

@Component({
  selector: 'app-contenedor',
  templateUrl: './contenedor.component.html',
  styleUrls: ['./contenedor.component.css']
})
export class ContenedorComponent implements OnInit {
  empleados:any[]= []

  constructor(private empleadoService:EmpleadoService) { }

  ngOnInit() {
    this.empleadoService.getEmpleados().subscribe(data=>{
      this.empleados.push(data)
    })
  }

  agregarEmpleado(empleado:any){
    const formData = new FormData();
    formData.append('brm',empleado.brm)
    formData.append('nombre',empleado.nombre)
    formData.append('puesto',empleado.puesto)
    formData.append('foto', empleado.foto)
    this.empleadoService.postEmpleado(formData).subscribe(data=>{
      console.log(this.empleados)
      this.empleados[0].push(data)
      console.log(this.empleados)
    })
  }

  eliminarEmpleado(empleado:any){
    this.empleados[0] = this.empleados[0].filter(t => t.id !== empleado.id)
    this.empleadoService.deleteEmpleado(empleado).subscribe()
  }
}
