import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

import { EmpleadoService } from 'src/app/services/empleado-service.service';

@Component({
  selector: 'app-empleados-table',
  templateUrl: './empleados-table.component.html',
  styleUrls: ['./empleados-table.component.css']
})
export class EmpleadosTableComponent implements OnInit {
  brm:string
  nombre:string
  puesto:string
  foto:any
  file:any

  @Input() empleados:any[]=[{
    brm:'',
    nombre:'',
    puesto:'',
    foto: ''
  }]

  display:boolean=false

  @Output() eliminarEmpleado: EventEmitter<any> = new EventEmitter()
  @Output() editarEmpleado: EventEmitter<any> = new EventEmitter()

  columns:string[] = ['Brm','Nombre','Foto del Empleado', 'Puesto', 'Acciones']

  constructor(private empleadoService:EmpleadoService) { }

  ngOnInit() {
  }

  borrar(empleado:any){
    this.eliminarEmpleado.emit(empleado)
  }

  editar(empleado){
    empleado.visible = !empleado.visible
    this.brm = empleado.brm
    this.nombre = empleado.nombre
    this.puesto = empleado.puesto
    this.editarEmpleado.emit()
  }

  editarForm(empleado){
    if(!this.foto){
      const url = `data:image/jpg;base64,${empleado.foto}`
      fetch(url)
      .then(res => res.blob())
      .then(blob => {
        this.file = new File([blob], "File name",{ type: "image/jpg" })
      })
    }
    const formData = new FormData();
    formData.append('brm',this.brm)
    formData.append('nombre',this.nombre)
    formData.append('puesto',this.puesto)
    if(this.foto) {
      formData.append('foto', this.foto) 
    }else{
      formData.append('foto', this.file) 
    }
    this.empleadoService.putEmpleado(empleado.id,formData).subscribe(data=>{
      const index = this.empleados[0].findIndex(x => x.id == empleado.id)
      this.empleados[0][index] = data
      this.file = ''
      this.foto=''
    })
  }

  onImageUpload(event){
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.foto = file
    }
  }
}