import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { AppComponent } from './app.component';
import { ContenedorComponent } from './components/contenedor/contenedor.component';
import { FormComponent } from './components/form/form.component';
import { EmpleadosTableComponent } from './components/empleados-table/empleados-table.component';

@NgModule({
  declarations: [
    AppComponent,
    ContenedorComponent,
    FormComponent,
    EmpleadosTableComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
