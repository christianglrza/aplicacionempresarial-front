import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  url:string = "https://aplicacion-empresarial.herokuapp.com/empleados"

  constructor(private http:HttpClient) { }

  getEmpleados():Observable<any[]>{
    return this.http.get<any[]>(this.url)
  }

  postEmpleado(empleado:any):Observable<any[]>{
    return this.http.post<any>(this.url,empleado)
  }

  deleteEmpleado(empleado:any):Observable<any>{
    const url = `${this.url}/${empleado.id}`
    return this.http.delete(url)
  }

  putEmpleado(id:any,empleado:any):Observable<any>{
    const url = `${this.url}/${id}`
    return this.http.put(url,empleado)
  }
}